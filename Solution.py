#from random import shuffle

"""
Given an array of integers greater than zero, 
find if it is possible to split it in two subarrays, 
such that the sum of the two subarrays is the same. 
Print the two subarrays.
"""

import itertools

class bcolors(object):

    """
    Class for printing out colours on the CLI
    """

    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class nocolors(object):

    """
    Class for when we don't want colours
    """

    HEADER = ''
    OKBLUE = ''
    OKGREEN = ''
    WARNING = ''
    FAIL = ''
    ENDC = ''
    BOLD = ''
    UNDERLINE = ''

class Solution:

    """
    We are going to work our test by stealing from the end of the first group
    using pop and then appending that integer into the second group so we can then compare the sum
    of the two lists after each iteration.

    We will use itertools.permutations() to get all the combinations of the list and produce a list of lists to test against with SplitArray().
    """

    def __init__(self):
        self.bcolors = bcolors()

    def get_list_of_lists(self, l):
        return list(itertools.permutations(tuple(l)))

    def SplitArray(self, input):
        lol = self.get_list_of_lists(input)
        for ray in lol:
            g1 = list(ray)
            g2 = []
            while len(g1) > 0:
                g2.append(g1.pop())
                if sum(g2) == sum(g1):
                    print(self.bcolors.HEADER + "Sum group 1 is %d and sum group 2 is %s and they are equal" % (sum(g1), sum(g2)) + self.bcolors.ENDC)
                    return True
        print(self.bcolors.FAIL + "No matching groups found" + self.bcolors.ENDC)
        return False

if __name__ == "__main__":
    rays = [[2, 5, 3],[ 1, 3, 3, 4, 5 ],[ 2, 4, 5, 6 ],[1,1],[1,2],[1,3,3,4,13,-2],[6,6,8]]
    sol = Solution()
    for ray in rays:
        print(bcolors.OKBLUE + "List of lists %s" % sol.get_list_of_lists(ray) + bcolors.ENDC)
        sol.SplitArray(ray)
