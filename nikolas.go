// Write a function which takes an array of integers and returns true if the elements of the array can be divided into 2 groups whose sums are equal.

// For example:

// Given the array [2, 5, 3] the function would return true.

// The array [2, 5, 3] can be split up into the two groups [5] and [2, 3] whose sums are both 5.


package Solution

func SplitArray(arr []int, n int) bool {

	// Find sum of whole array
	sum := 0
	for i := 0; i < n; i++ {
		sum += arr[i]
	}

	// If sum of array is not even than we can not divide
	
	if sum%2 != 0 {
		return false
	}

	sum = sum / 2

	// For each element arr[i], see if there is another element 
	var s []int
	for i := 0; i < n; i++ {
		val := sum - arr[i]

		// If element exist  return  true
		if contains(s, val) {
		
			return true
		}
        // otherwise append to new array for next check
    	s = append(s, arr[i])
	}
	return false
}

func contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}