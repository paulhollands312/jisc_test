from Solution import Solution
import unittest

class SolutionTests(unittest.TestCase):


	def test1(self):
		solution = Solution()
		self.assertEqual(solution.SplitArray([ 1, 3, 3, 4, 5 ]), True)

	def test2(self):
		solution = Solution()
		self.assertEqual(solution.SplitArray([ 2, 4, 5, 6 ]), False)

if __name__ == '__main__':
	unittest.main()
